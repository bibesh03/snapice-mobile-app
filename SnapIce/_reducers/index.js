import {combineReducers} from 'redux';
import AlertReducers from './alert.reducer';
import ForgotPasswordReducers from './Login/ForgotPassword.reducer'

export default combineReducers({
    AlertReducers,
    ForgotPasswordReducers
})