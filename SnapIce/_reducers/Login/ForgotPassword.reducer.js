import forgotPasswordConstants from '../../_constants/forgotpassword.constants';
import {combineReducers} from 'redux';


const forgotPasswordInitialModal = {
    isOpen: false
}

const forgotPasswordModal = (state = forgotPasswordInitialModal, action) => {
    switch(action.type) {
        case forgotPasswordConstants.SHOW_MODAL:
            return {
                isOpen: true
            }
        case forgotPasswordConstants.HIDE_MODAL:
            return {
                isOpen: false
            }
        default:
            return state;
    }
}

const ForgotPasswordReducers = combineReducers({
    forgotPasswordModal
});

export default ForgotPasswordReducers;