import {alertConstants} from '../_constants/alert.constants';
import {combineReducers} from 'redux';

var initialAlertState = {
    alertType: 'default',
    place: "tc",
    color: "success",
    message: "Howdy User",
    open: false
}

const alert = (state = initialAlertState, action) => {
    switch(action.type) {
        case  alertConstants.SHOW_ALERT:
            return {
                ...state,
                alertType: action.alertType,
                place: action.place,
                color: action.color,
                message: action.message,
                open: true
            };
        case alertConstants.HIDE_ALERT:
            return {
                ...state,
                open: false
            };
        default:
            return state;
    }
}

const AlertReducers = combineReducers({
    alert
});

export default AlertReducers;