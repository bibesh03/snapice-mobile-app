import forgotPasswordConstants from '../../_constants/forgotpassword.constants';

export const showForgotPasswordModal = () => ({
    type: forgotPasswordConstants.SHOW_MODAL
})
  
export const hideForgotPasswordModal = () => ({
    type: forgotPasswordConstants.HIDE_MODAL
})