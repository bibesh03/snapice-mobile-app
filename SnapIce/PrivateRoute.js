import React, {useState, useEffect} from 'react';
import {Route, Redirect} from 'react-router-dom';
import { View, StatusBar, StyleSheet, Image, AsyncStorage } from "react-native";
import {Text, Body, Title, Right, Icon, Container, Header, H2, Button} from 'native-base';

const NavBar = (props) => {
    return (
        <Header>
            <Body>
                <Title>Snap Ice</Title>
            </Body>
            <Right>
                <Button title="what" transparent onPress={() => {
                    AsyncStorage.removeItem('user');
                    props.history.push('/');
                }}>
                    <Icon type="SimpleLineIcons" active name='logout' />
                        <Text>Log out</Text>
                </Button>
            </Right>
        </Header>
    )
}

export const PrivateRoute = ({component: Component, ...rest}) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [checkedSignIn, setCheckedSignIn] = useState(false);

    useEffect(() => {
        isSignedIn().then(res => {
            setIsLoggedIn(res);
            setCheckedSignIn(true);
         })
    }, [setIsLoggedIn]);

    if (!checkedSignIn) {
        return null;
    }

    return (
        <Route {...rest} render={props => (
            isLoggedIn ? 
            <View style={{width: '100%', height: '100%', marginTop: StatusBar.currentHeight }}>
                <NavBar {...props}/>
                <Component {...props} />
            </View>
             : <Redirect to ={{pathname: '/', state: {from: props.location}}} />
        )}/>
    )
}

export const NormalRoute = ({component: Component, ...rest}) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [checkedSignIn, setCheckedSignIn] = useState(false);

    useEffect(() => {
        isSignedIn().then(res => {
            setIsLoggedIn(res);
            setCheckedSignIn(true);
         })
    }, [setIsLoggedIn, setCheckedSignIn]);

    if (!checkedSignIn) {
        return null;
    }

    return (
            <Route {...rest} render={props => (
                isLoggedIn ? 
                <View style={{width: '100%', height: '100%', marginTop: StatusBar.currentHeight }}>
                    <NavBar {...props}/>
                    <Redirect to ={{pathname: '/Home', state: {from: props.location}}} />
                </View>
                : <Component {...props} />
        )}/>
    )
}

const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('user').then(res => {
            let storedUser = JSON.parse(res);
            if (storedUser) {
                resolve(true)
            } else {
                resolve(false)
            }
        })
    });
}