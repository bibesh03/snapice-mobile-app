import React, {useState, useEffect} from "react";
import {connect} from 'react-redux';
import { withRouter } from "react-router-dom";
import { View, Image, StyleSheet } from "react-native";
import { H2, H1, Button, H3, Text} from 'native-base';
import * as Permissions from 'expo-permissions';

const ScanToBegin = (props) => {
    return (
        <View style={styles.container}>
            <H1 style={{ marginBottom: '2.3%'}}>WELCOME TO</H1>
            <Image
                style={{width: 250, height: 50, marginBottom: '5%'}}
                source={require('../../assets/img/logo-text.png')}
            />
            <Image
                style={{ marginBottom: '5%'}}
                source={require('../../assets/img/center-logo.png')} 
            />
            <H2 style={{ marginBottom: '2.3%'}}>Hi, Click scan to begin</H2>
            <Button bordered primary style={{ width: '60%', justifyContent: 'center'}} onPress={() => props.history.push('/QRScanner')}> 
                <Text> Scan </Text>
            </Button>
        </View>
    );
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      paddingTop: '10%'
    }
});
  
export default withRouter(connect(null, null)(ScanToBegin))