import React, {useState, useEffect} from "react";
import {connect} from 'react-redux';
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withRouter } from "react-router-dom";
import {StyleSheet, View, ImageBackground, Image } from "react-native";
import {Text, Body, Content, Form, Item, Input, Label, Icon, Button, Toast } from 'native-base';
import {KeyboardAvoidingView} from 'react-native';
import {AsyncStorage} from 'react-native';
import {showForgotPasswordModal, hideForgotPasswordModal} from '../../_actions/Login/forgotpassword.action';
import ForgotPasswordModal from '../../components/Login/ForgotPasswordModal';

const Login = (props) => {
    const keyboardVerticalOffset =  0

    const[ email, setEmail ] = useState('');
    const[ emailError, setEmailError] = useState(false);
    const[ password, setPassword ] = useState('');
    const[ passwordError, setPasswordError] = useState(false);

    const {showForgotPasswordModal, hideForgotPasswordModal, modalState} = props;

    useEffect(() => {
        if (email != "") {
            setEmailError(!checkEmailError())
        }
    }, [email]);

    const submitLoginForm = () => {
        if (email != "" && password != "" && !emailError && !passwordError) {
            AsyncStorage.setItem('user', JSON.stringify({name: "test"})).then((res) => {
                props.history.push("/home");
            }).catch( ()=>{
                console.log('There was an error saving the product');
            } )
        } else {
            Toast.show({
                text: "Please enter valid data before submitting!",
                buttonText: "Okay",
                position: "top",
                type: 'danger'
            })}
    }

    const checkEmailError = () => {
        if (email === "") 
            return false;

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        return re.test(String(email).toLowerCase());
    }

    return (
        //uri: 'https://images.pexels.com/photos/281260/pexels-photo-281260.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
        <ImageBackground source={require('../../assets/img/clip-art.png')} style={{width: '100%', height: '100%', clipPath: 'polygon(10% 36%, 100% 1%, 100% 100%, 25% 100%, 85% 88%)'}}>
            <Content>
                <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={keyboardVerticalOffset}>
                    <View style={{ marginTop: '85%'}}>
                        <Image
                            style={{width: 150, height: 150, marginLeft: '32.5%'}}
                            source={require('../../assets/img/mobile-app.png')} 
                        />
                        <Form style={{ paddingLeft: '5%', paddingRight: '5%'}}>
                            <Item floatingLabel error={emailError}>
                                <Label style={{ color: 'white'}}>Email</Label>
                                <Icon  type="MaterialIcons" active name='email' style={{ color: 'white'}}/>
                                <Input
                                    onChangeText={email => {
                                        setEmail(email);
                                    }}
                                    onBlurText={email => {
                                        setEmail(email);
                                    }}
                                />
                            </Item>
                            <Item floatingLabel error={passwordError}>
                                <Label style={{ color: 'white'}}>Password</Label>
                                <Icon type="MaterialIcons" active name='lock' style={{ color: 'white'}}/>
                                <Input
                                    onChangeText={pwd => {
                                        setPassword(pwd);
                                        setPasswordError(pwd == "")
                                    }}
                                    onBlurText={pwd => {
                                        setPassword(pwd);
                                        setPasswordError(pwd == "")
                                    }}
                                    secureTextEntry={true}
                                />
                            </Item>
                            <View style={{alignItems: 'flex-end'}}>
                                <Button transparent onPress={()=>showForgotPasswordModal()}>
                                    <Text>Forget Password?</Text>
                                </Button>
                            </View>
                            <Body style={{ marginTop: 15 }}>
                                <Button iconRight block style={{backgroundColor: '#ffcc77'}} onPress={()=> submitLoginForm()}>
                                    <Text>Login</Text>
                                    <Icon name="arrow-forward"/>
                                </Button>
                            </Body>
                        </Form>
                        <ForgotPasswordModal/>
                    </View>
                </KeyboardAvoidingView>
            </Content>
        </ImageBackground>
    );
    
}
  
const mapStateToProps = (state) => ({
    modalState: state.ForgotPasswordReducers.forgotPasswordModal,
});
  
const mapDispatchToProps = {
    showForgotPasswordModal,
    hideForgotPasswordModal
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))