import React, {useState, useEffect} from "react";
import {connect} from 'react-redux';
import { withRouter } from "react-router-dom";
import { View, StatusBar, StyleSheet, Image, Text } from "react-native";
import {Body, Title, Right, Icon, Button, Container, Header, Footer, FooterTab, Spinner, Content } from 'native-base';
import { AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { BackHandler } from 'react-native';

import { BarCodeScanner } from 'expo-barcode-scanner';

const QRScanner = (props) => {
    const [hasCameraPermission, setHasCameraPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect (() => {
        this.getPermissionAsync();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        return() => {
            BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
        }
    }, [])

    handleBackButtonClick = () => {
        props.history.goBack(null);
        return true;
    }
    
    getPermissionAsync = async () => {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        setHasCameraPermission(status === 'granted');
    }

    handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    };
    if (hasCameraPermission === null) {
        return <View style={styles.container}><Content><Spinner color='blue' /><Text>Requesting for camera permission..</Text></Content></View>;
    }
    
    if (hasCameraPermission === false) {
    return <Text>No access to camera</Text>;
    }

    return (
            <View
                style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'flex-end',
                maxHeight: '90%',
            }}>
                <BarCodeScanner
                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                    style={StyleSheet.absoluteFillObject}
                />
                <Footer>
                <FooterTab>
                    <Button danger onPress={() => handleBackButtonClick()}>
                    <Text style={{ color: 'white'}}>Go back </Text>
                    </Button>
                </FooterTab>
            </Footer>
            </View>
    );
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      paddingTop: '10%'
    }
  });
  
  
export default withRouter(connect(null, null)(QRScanner))