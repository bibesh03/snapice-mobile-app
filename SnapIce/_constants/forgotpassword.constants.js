const forgotPasswordConstants = {
    SHOW_MODAL: 'SHOW_MODAL',
    HIDE_MODAL: 'HIDE_MODAL'
}

export default forgotPasswordConstants;