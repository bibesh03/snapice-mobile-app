import React, { Component } from 'react';
import { StyleSheet, View  } from 'react-native';
import rootReducer from './_reducers/index';
import { createStore } from 'redux';
import { createMemoryHistory } from 'history';
import { Router, Route, Switch } from "react-router-dom";
import Login from './views/Login/Login.js';
import { AppLoading } from "expo";
import * as Font from 'expo-font';
import {Root} from "native-base";
import ScanToBegin from "./views/Home/ScanToBegin";
import QrScanner from "./views/QRScanner/QRScanner";

const store = createStore(rootReducer);
import { Provider } from 'react-redux';
import {PrivateRoute, NormalRoute} from './PrivateRoute';

const hist = createMemoryHistory();

export default class App extends Component {
  state = {
    fontLoaded: false
  };

  async componentWillMount() {
    try {
      await Expo.Font.loadAsync({
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      });
      this.setState({ fontLoaded: true });
    } catch (error) {
      console.log('error loading icon fonts', error);
    }
  }

  render() {
    if (!this.state.fontLoaded) {
      return <AppLoading />;
    }
    return (
      <Provider store={store}>
        <Router history={hist}>
          <Root>
            <View style={styles.container}>
              <Switch>
                <NormalRoute exact path="/" component={Login}/>
                <PrivateRoute exact path="/Home" component={ScanToBegin}/>
                <PrivateRoute excat path="/QrScanner" component={QrScanner}/>
              </Switch>
            </View>
          </Root>
        </Router>
      </Provider>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
