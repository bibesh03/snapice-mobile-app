import React, {Component} from 'react';
import {View, Button} from 'react-native';
import {showForgotPasswordModal, hideForgotPasswordModal} from '../../_actions/Login/forgotpassword.action';
import { withRouter } from "react-router-dom";
import {connect} from 'react-redux';
import Modal from "react-native-modal";
import {Item, Label, Icon, Input, Container, Header, Content, Card, CardItem, Body, Text, Right, Left, H3} from "native-base";

const ForgotPasswordModal = (props) => {
  const {showForgotPasswordModal, hideForgotPasswordModal, modalState} = props;

    return (
      <View style={{ maxHeight: 34}}>
        <Modal
          isVisible={modalState.isOpen}
          backdropColor='white'
          backdropOpacity={.7} 
          onBackButtonPress={() => hideForgotPasswordModal()}
          onSwipeComplete ={() => hideForgotPasswordModal()}
          swipeDirection="right">
            <Header>
              <Left>
                <Text style={{color:'white'}}>Reset Password</Text> 
              </Left>
              <Right>
                <Icon onPress={hideForgotPasswordModal} type="MaterialIcons" active name='clear' style={{ color: 'white'}}/>
              </Right>
            </Header>
            <Content style={{ backgroundColor: 'white', maxHeight: '40%'}} padder>
                <H3 style={{color: 'green'}}>Can't Log in?</H3>
                <Text>
                  Here’s the info to get you back in to your account as quickly as possible.
                </Text>
                <Text>
                  First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off,
                  and that your username is spelled correctly, and then try again.
                </Text>
                <Text style={{ marginBottom: 10}}>
                  If your password still isn’t working, it’s time to reset your password.
                </Text>
                <Item floatingLabel style={{ marginBottom: 20}}>
                  <Label style={{ color: 'black'}}>Email</Label>
                  <Icon type="MaterialIcons" active name='email' style={{ color: 'black'}}/>
                  <Input />
                </Item>
                  <Button color='green' title="Reset Password" danger onPress={hideForgotPasswordModal} />
            </Content>
        </Modal>
      </View>
    );
}

const mapStateToProps = (state) => ({
  modalState: state.ForgotPasswordReducers.forgotPasswordModal,
});

const mapDispatchToProps = {
  showForgotPasswordModal,
  hideForgotPasswordModal
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordModal))